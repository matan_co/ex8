<?php

use yii\db\Migration;

class m170612_124135_alter_user_and_lead_tables extends Migration
{
    public function up()
    {
		$this->addColumn('user','firstname','string');
		$this->addColumn('user','lastname','string');
    }

    public function down()
    {
        echo "m170612_124135_alter_user_and_lead_tables cannot be reverted.\n";

		$this->dropColumn('user','firstname');
		$this->dropColumn('user','lastname');
        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
